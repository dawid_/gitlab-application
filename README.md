# Crontab 

```
0 * * * * 
 ~/cron_exec.sh >/dev/null 2>&1
```

# Log file location

```
pwd ; ls | grep user
/var/log
current_users
user_changes
```

# Print users and their home directories

```
./print_users_homedir.sh 
root:/root
daemon:/usr/sbin
bin:/bin
sys:/dev
sync:/bin
games:/usr/games
man:/var/cache/man
lp:/var/spool/lpd
mail:/var/mail
news:/var/spool/news
uucp:/var/spool/uucp
proxy:/bin
www-data:/var/www
backup:/var/backups
list:/var/list
irc:/var/run/ircd
gnats:/var/lib/gnats
nobody:/nonexistent
_apt:/nonexistent
nginx:/nonexistent
sshd:/run/sshd
statd:/var/lib/nfs
app:/mnt
web:/app
```

# Current users has

```
cat current_users
4c3a48aa931d2dfdb2e63dafe9f81d2e  -
```

# User changes

```
cat user_changes                     
Sat  4 Dec 2021 20:49:06 GMT changes occured
```

# print_users_homedir.sh code

```
#!/bin/bash
inputFile="/etc/passwd"
while IFS=: read -r username password userid groupid commend homedir cmdshell
do
        echo "$username:$homedir"
done < $inputFile
```

# cron_exec.sh code

```
#!/bin/bash

# calculate md5 of output and save to temp file
userListMd5=$(sh  ~/print_users_homedir.sh | md5sum > /tmp/tmp_changes)

# file vars
originalHash="/var/log/current_users"
logFile="/var/log/user_changes"
compareChanges="/tmp/tmp_changes"


if ! cmp --silent "$originalHash" "$compareChanges";
then
        echo "Changes detected"
        echo "$(date) changes occured" > $logFile
else
        echo "Changes not detected"
fi
# update current users hash
exec  ~/print_users_homedir.sh | md5sum > /var/log/current_users
```
