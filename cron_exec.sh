#!/bin/bash

# calculate md5 of output and save to temp file
userListMd5=$(sh ~/print_users_homedir.sh | md5sum > /tmp/tmp_changes)

# file vars
originalHash="/var/log/current_users"
logFile="/var/log/user_changes"
compareChanges="/tmp/tmp_changes"

if ! cmp --silent "$originalHash" "$compareChanges";
then
        echo "Changes detected"
        echo "$(date) changes occured" > $logFile
else
        echo "Changes not detected"
fi
# update current users hash
echo exec ~/print_users_homedir.sh | md5sum > /var/log/current_users
